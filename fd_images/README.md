# Floppy Disk Images

Images created with [ImageDisk 1.18](http://www.classiccmp.org/dunfield/img/index.htm).

File(s)                 | Picture                                      | Contents
------------------------|----------------------------------------------|-----------
BLANK.IMD               |                                              | Disk formatted on IBM 5322 with format utility on DIAG2.IMD. Volume ID: 123456 Note: Either SS or DS media gets formatted single-sided?
DIAG1.IMD               | [![](DIAG1_small.jpg?raw=true)](DIAG1.jpg)   | System 23 Diagnostic Diskette PN 6841645 EC 994445 04/06/82 1417
DIAG2.IMD               | [![](DIAG2_small.jpg?raw=true)](DIAG2.jpg)   | 5322 -SYSTEM- DIAGNOSTIC DISKETTE PN 6841645 EC 334606 06/07/83 0517
LEARN.IMD               | [![](LEARN_small.jpg?raw=true)](LEARN.jpg)   | IBM SYSTEM/23   VOL003 LEARNING DISK PART NO. 4498428 EC 997208 05/19/83 0519 ENGLISH
LRN004.IMD              | [![](LRN004_small.jpg?raw=true)](LRN004.jpg) | LRN004 BACKUP LEARNING
MISC01.IMD - MISC06.IMD | [![](MISC_small.jpg?raw=true)](MISC.jpg)     | Misc. unlabeled disk found with other System/23 disks.
TXSYS2.IMD              | [![](TXSYS2_small.jpg?raw=true)](TXSYS2.jpg) | TXSYS2 VOL 1 OF 1 1985 TAX-1040 SYSTEM TO INSTALL, KEY: PROC TX.INSTALL (Original disk had bad sectors)
