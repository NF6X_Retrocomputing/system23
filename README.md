# IBM System/23 Datamaster

This is a collection of documentation and disk images related to the IBM System/23 Datamaster computer system. Information about this system is fairly sparse online, so please contribute any other related documents and disk images which are appropriate to this collection.

![](system23.jpg?raw=true)
